# Software Studio 2021 Spring Assignment 2


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|10%|Y|
|Complete Game Process|5%|Y|
|Basic Rules|45%|Y|
|Animations|10%|Y|
|Sound Effects|10%|Y|
|UI|10%|Y|

## Website Detail Description
# Basic Components Description : 
### Play at 120fps for the best gaming experience
1. World map : This game has two stages
For stage 1: We have a mushroom, a coin, an enemy flower,and an enemy turtle.
For stage 2: We have a coin, an enemy flower, and three enemy goomba.

2. Player : Use Left/Right and space to control the Player.
If it touches an enemy and you eat a mushroom beforehand, it become smaller and become super (immune any injury) for 3 seconds. If you didn't eat a mushroom, you died.

3. Enemies
Flower : Move up and down, Player cannot touch it.
Goomba : Move left and right, you can kill it by stepping on its head, otherwise it'll kill you.
Turtle : Move left and right, you can kill it by stepping on its head, and you can kick it to let it move again. If you didn't kill it first, it'll kill you if you want to kick it.
4. Question Blocks : There are two kinds of question blocks : coin and mushroom.
Mushroom can let you grow up and immune one injury.
Coin can have a big bonus on the score.
5. Animations : 
Player : die, jump, walk, idle ,success.
Goomba : walk and die.
Turtle : walk, die, spin(after kicking).
Flower : mouth open and close.
Mushroom : Grow and move.
Coin : Get.
6. Sound effects : 
Player : die, jump, success, powerup, powerdown, stomp.
Turtle : spin(after kicking).
Mushroom : Grow.
Coin : Get.
Flag : win.
7. UI : 
Life : 3
Time : 100
Final Score = Life*1000 + Time * 10+ enemykilled * 100 + coin * 1000